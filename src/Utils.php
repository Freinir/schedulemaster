<?php
namespace Freinir\ScheduleMaster;

/**
 * Class Utils
 * @package Freinir\ScheduleMaster
 */
class Utils {
    private const DAY_KEYS = Scheduler::DAY_KEYS;
    private const MIDNIGHT = '00:00';

    private const MINUTES_IN_HOUR = 60;
    private const MINUTES_IN_DAY = 60 * 24;

    private const BREAK_LUNCH = Scheduler::BREAK_LUNCH;
    private const BREAK_REST = Scheduler::BREAK_REST;

    private const EVENT_OPEN = Scheduler::EVENT_OPEN;
    private const EVENT_CLOSE = Scheduler::EVENT_CLOSE;

    /**
     * Returns key for next day with offset from current day
     * @param  string day - current day
     * @param  integer offset
     * @return string
     */
    function getDayFromToday($day, $offset) {
        return self::DAY_KEYS[(array_search($day, self::DAY_KEYS) + $offset) % 7];
    }


    /**
     * Returns interval that intersect current day with next day.
     * For schedule '10:00–13:00, 14:00–02:00' returns '00:00–02:00'
     * @param  array working_hours
     * @return array
     */
    function getOverlappingWorkingHours($working_hours) {
        $interval = [];

        foreach($working_hours as $value) {
            if($value['from'] > $value['to'])
                $interval = $value;
        }

        if(count($interval)){
            return [
                'from'  => self::MIDNIGHT,
                'to'    => $interval['to']
            ];
        }

        return null;
    }

    /**
     * Checks interval contains passed time
     * @param  string  time
     * @param  array  interval
     * @return bool
     */
    function isTimeInInterval($time, $interval) {
        $from = $interval['from'];

        // 23:00–00:30 --> 23:00–24:30
        $to = $interval['to'] > $interval['from'] ? $interval['to'] : $this->add24h($interval['to']);
        return ($time >= $from && $time < $to);
    }

    /**
     * '01:10' --> '25:10'
     * @param  string time
     * @return string
     */
    function add24h($time) {
        $parts = explode(':', $time);
        return ((int)$parts[0] + 24) . ':' . $parts[1];
    }

    /**
     * Returns interval, that contents passed time
     * @param  array intervals - array of intervals
     * @param  string time
     * @return array
     */
    function findInterval($intervals, $time) {
        $interval=[];

        foreach ($intervals as $value) {
            if(count($interval))
                break;
            if($this->isTimeInInterval($time, $value))
                $interval = $value;
        }
        return $interval;
    }


    /**
     * Returns true, if schedule the same for every day
     * @param  array schedule
     * @return bool
     */
    function isEveryDay($schedule) {
        $worksEveryDay = true;
        $day0 = @serialize($schedule[self::DAY_KEYS[0]]);

        foreach (self::DAY_KEYS as $key) {
            if(@serialize($schedule[$key]) != $day0)
                return false;
        }
        return $worksEveryDay;
    }


    /**
     * Returns array of breaks between work time intervals
     * If work time is: [{ from: '08:00', to: '13:00' }, { from: '14:00', to: '17:45' }, { from: '18:00', to: '20:00' }]
     * Breaks will be [{ from: '13:00', to: '14:00' }, { from: '17:45', to: '18:00' }]
     * @param  array working_hours
     * @return array
     */
    function getBreakHours($working_hours) {
        if(count($working_hours) == 1)
            return [];

        $data = [];
        $break = [];
        foreach ($working_hours as $item) {
            if(isset($break['from'])) {
                $break['to'] = $item['from'];
                $data[] = $break;
                unset($break);
            } else
                $break['from'] = $item['to'];
        }

        return $data;
    }


    /**
     * Returns type of break. Break for lunch, or other break
     * 13:00–14:00 --> lunch
     * 17:00—17:45 --> break
     * @param  array interval
     * @param  string interval.from - break beginning
     * @param  string interval.to - break end
     * @return string
     */
    function getBreakType($interval) {
        $to = $interval['to'] != self::MIDNIGHT ? $interval['to'] : $this->add24h($interval['to']); // 00:00 --> 24:00

        return $interval['from'] >= '12:00' && $to <= '16:00' ? self::BREAK_LUNCH : self::BREAK_REST;
    }


    /**
     * Returns break time between events
     * @param  {Object} event1 - first event
     * @param  {Object} event2 - next event
     * @return {String}
     */
    function getBreakTypeBetweenEvents($event1, $event2) {
        // If days different it isn't common break
        if ($event1['dayOffset'] != $event2['dayOffset']) {
            return null;
        }

        return $this->getBreakType([
            'from'=> $event1['time'],
            'to'=> $event2['time']
        ]);
    }



    /**
     * Returns array of interval events — openings and closings
     * @param  array [interval]
     * @return array [[type: string, time: string, dayOffset: string]]
     */
    function splitIntervalToEvents($interval) {
        if (!$interval) {
            return [];
        }

        $over = $interval['to'] < $interval['from']; // interval cross midnight

        return [
            [
                'type'=> self::EVENT_OPEN,
                'time'=> $interval['from'],
                'dayOffset'=> ($interval['dayOffset'] ?? 0)
            ],
            [
                'type' => self::EVENT_CLOSE,
                'time' => $interval['to'],
                'dayOffset' => ($interval['dayOffset'] ?? 0) + (int)$over
            ]
        ];
    }


    /**
     * Gap between two timestamps with dayOffset in minutes
     * @param  string fromTime
     * @param  string toTime
     * @param  string dayOffset
     * @return integer
     */
    function timeTo($fromTime, $toTime, $dayOffset) {
        $from = $this->parseTime($fromTime);
        $to = $this->parseTime($toTime);
        return ($to['m'] - $from['m']) + ($to['h'] - $from['h']) * self::MINUTES_IN_HOUR + $dayOffset * self::MINUTES_IN_DAY;
    }


    /**
     * 01:23 --> { h: 1, m: 23 }
     * @param  string time
     * @return array
     */
    function parseTime($time) {
        $parts = explode(':', $time);
        return [
            'h' => +$parts[0],
            'm' => +$parts[1]
        ];
    }


    /**
     * Returns working_hours array from schedule for one day
     * @param array scheduleForDay - schedule object for one day like this: { working_hours: [{ from: '06:00', to: '07:00' }] }
     * @param array defaultValue - what return if schedule id empty
     * @return array
     */
    function getWorkingHours($scheduleForDay, $defaultValue) {
        if(!is_array($scheduleForDay))
            return $defaultValue;
        return count($scheduleForDay) ? $scheduleForDay['working_hours'] : $defaultValue;
    }

}