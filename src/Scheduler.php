<?php
namespace Freinir\ScheduleMaster;

/**
 * Class Scheduler
 * @package Freinir\ScheduleMaster
 */
class Scheduler {
    public const DAY_KEYS = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
    public const RU_WEEKDAYS_SHORT = ['Mon' => 'ПН', 'Tue'=>'ВТ', 'Wed'=>'СР', 'Thu'=>'ЧТ', 'Fri'=>'ПТ', 'Sat'=>'СБ', 'Sun' => 'ВС'];
    public const WORK_EVERYDAY = 'EVERYDAY';
    public const WORK_24x7 = '24x7';
    public const WORK_NOT_WORKING = 'NOT_WORKING';

    public const BREAK_LUNCH = 'LUNCH';
    public const BREAK_REST = 'REST';

    public const EVENT_OPEN = 'OPEN';
    public const EVENT_CLOSE = 'CLOSE';

    public const STATUS_OPENED = 'OPENED';
    public const STATUS_WILL_CLOSE_IN_TIME_FOR_BREAK = 'WILL_CLOSE_IN_TIME_FOR_BREAK';
    public const STATUS_WILL_CLOSE_IN_TIME = 'WILL_CLOSE_IN_TIME';

    public const STATUS_WILL_OPEN_AT_TIME = 'WILL_OPEN_AT_TIME';
    public const STATUS_WILL_OPEN_AT_DAY_AT_TIME = 'WILL_OPEN_AT_DAY_AT_TIME';
    public const STATUS_WILL_OPEN_TOMORROW_AT_TIME = 'WILL_OPEN_TOMORROW_AT_TIME';
    public const STATUS_WILL_OPEN_DAY_AFTER_TOMORROW_AT_TIME = 'WILL_OPEN_DAY_AFTER_TOMORROW_AT_TIME';

    public const STATUS_WILL_OPEN_IN_TIME = 'WILL_OPEN_IN_TIME';

    public const STATUS_WILL_OPEN_IN_TIME_FROM_BREAK = 'WILL_OPEN_IN_TIME_FROM_BREAK';
    public const STATUS_WILL_OPEN_AT_TIME_FROM_BREAK = 'WILL_OPEN_AT_TIME_FROM_BREAK';

    /**
     * @var Utils $utils
     */
    private $utils;

    public function __construct()
    {
        $this->utils = new Utils();
    }

    /**
     * Returns working hours for current day with working hours before closing on previous day.
     * @param  {Object} schedule
     * @param  {String} key - day key (Mon/Tue/Wed/...)
     * @return array
     */
    function getDayWithOverlap($schedule, $key) {
        $prev = $this->utils->getDayFromToday($key, 6);
        $overlap = $schedule[$prev] &&  $this->utils->getOverlappingWorkingHours($schedule[$prev]['working_hours']);

        $working_hours = $this->utils->getWorkingHours($schedule[$key], []);

        return [
            'working_hours' => $overlap ? [$overlap, $working_hours] : $working_hours
        ];
    }

    /**
     * Open or close?
     * @param  array schedule
     * @param  array now
     * @return bool
     */
    function isOpened($schedule, $now) {
        if ($this->is24x7($schedule)) {
            return true;
        }

        $currentDay = $this->getDayWithOverlap($schedule, $now['day']);
        $interval = $this->utils->findInterval($currentDay['working_hours'], $now['time']);

        // If for now have interval that opened, else - closed
        return !!$interval;
    }

    /**
     * @param  array schedule
     * @return bool
     */
    function is24x7($schedule) {
        $works24x7 = true;

        foreach (self::DAY_KEYS as $day) {
            $oneDaySchedule = isset($schedule[$day]) ? $schedule[$day] : false;
            if (!$oneDaySchedule || !$oneDaySchedule['working_hours'] || count($oneDaySchedule['working_hours']) != 1) {
                $works24x7 = false;
                return $works24x7;
            }

            $interval = $oneDaySchedule['working_hours'][0];
            if (!($interval && $interval['from'] == '00:00' && $interval['to'] == '24:00')) {
                $works24x7 = false;
            }
        }
        return $works24x7;
    }

    /**
     * Returns today worktime and it's type
     * Type: "24x7", "all day the same", or "today".
     * @param  array schedule
     * @param  array now
     * @return array
     */
    function getTodayWorktime($schedule, $now) {
        if ($this->is24x7($schedule)) {
            return [
                'type' => self::WORK_24x7
            ];
        }

        $today = $schedule[$now['day']];

        if (!$today) {
            return [
                'type'=> self::WORK_NOT_WORKING
            ];
        }

        return [
            'type' => $this->utils->isEveryDay($schedule) ? self::WORK_EVERYDAY : self::WORK_TODAY,
            'intervals' => $today['working_hours']
        ];
    }

    /**
     * Returns array of breaks for today
     * @param  array schedule
     * @param  array now
     * @return array
     */
    function getTodayBreakHours($schedule, $now) {
        return $this->utils->getBreakHours($this->utils->getWorkingHours($schedule[$now['day']], []));
    }

    /**
     * Search next 2 events (open/close) and their time
     * @param  array schedule
     * @param  array now
     * @return array
     */
    function getNextEvents($schedule, $now) {
        // Build new array, that starts from today and ends the same day in next week
        // For friday it will be like this ['Fri', 'Sat', 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri']
        // Second friday need for case when firm works only on Friday for looking next event in this day
        $nowDayIndex = array_search($now['day'], self::DAY_KEYS);

        $keysFromToday = array_merge( array_slice(self::DAY_KEYS, $nowDayIndex),array_slice(self::DAY_KEYS, 0, $nowDayIndex+1));

        // We need take events from yesterday that happens today
        $yesterdayKey = $this->utils->getDayFromToday($now['day'], 6);
        if(isset($schedule[$yesterdayKey])) {
            $yesterdayWorkingHours = $this->utils->getWorkingHours($schedule[$yesterdayKey], []);
            $yesterdayEvents = $this->utils->splitIntervalToEvents(
                $this->utils->getOverlappingWorkingHours($yesterdayWorkingHours)
            );
        } else {
            $yesterdayEvents = [];
        }

        // Sorting schedule
        $workTimes = [];
        foreach ($keysFromToday as $key) {
            @$workTimes[] = $schedule[$key];
        }

        // Adding offset from today to have this information after flatten
        $valet = [];
        foreach ($workTimes as $dayIndex => $workTime) {
            $wh = $this->utils->getWorkingHours($workTime, []);
            foreach ($wh as $part){
                $valet[] = [
                    'from' => $part['from'],
                    'to'    => $part['to'],
                    'dayOffset' => $dayIndex
                ];
            }
        }

        // Transform each interval in two events: closing and opening
        $king = array_slice($yesterdayEvents, 1);
        foreach ($valet as $key => $value) {
            $item = $this->utils->splitIntervalToEvents($value);
            $king = array_merge($king, $item);
        }

        $king = array_filter($king, function ($value) use ($now) {
            return $value['dayOffset'] > 0 || $value['time'] > $now['time'];
        });

        return array_slice($king, 0,2);
    }



    /**
     * State of the schedule with forecast like this:
     * "Will open tomorrow at 10:00" / "Will open on Monday at 8:00" but in object with constants instead of text
     * @param  array schedule
     * @param  array now [ 'day' => 'Mon', 'time' => '00:05' ]
     * @param  {Number} forecastThreshold - how max minutes should left when we should say about next event (open / close)
     * @param  array weekends - days that usually is day off ['Sat', 'Sun']
     * @return array
     */
    function getStatus($schedule, $now, $forecastThreshold, $weekends) {

        if ($this->is24x7($schedule)) {
            return [
                'type' => self::STATUS_OPENED
            ];
        }

        $nextStatusChangeEvents = $this->getNextEvents($schedule, $now);

        if (count($nextStatusChangeEvents) < 2) {
            return null;
        }

        $next = $nextStatusChangeEvents[0];
        $overnext = $nextStatusChangeEvents[1];
        $minutesTo = $this->utils->timeTo($now['time'], $next['time'], $next['dayOffset']);

        // If open now and will close
        if ($next['type'] == self::EVENT_CLOSE) {
            // If enough time before closing only say that opened
            if ($minutesTo >= $forecastThreshold) {
                return [
                    'type' => self::STATUS_OPENED
                ];
            }
            // If break/lunch near say what type of break it will be
            $breakType = $this->utils->getBreakTypeBetweenEvents($next, $overnext);

            return [
                'type' => $breakType ? self::STATUS_WILL_CLOSE_IN_TIME_FOR_BREAK : self::STATUS_WILL_CLOSE_IN_TIME,
                'breakType' => $breakType,
                'minutesTo' => $minutesTo
            ];
        }

        // next.type == EVENT_OPEN
        $days = $next['dayOffset']; // days before opening
        $isWeekendToday = array_search($now['day'], $weekends); // if it is day off today

        // will open today (first open or from break)
        if ($days == 0) {
            $breakhours = $this->getTodayBreakHours($schedule, $now);
            $currentBreak = $this->utils->findInterval($breakhours, $now['time']);
            $breakType = $currentBreak && $this->utils->getBreakType($currentBreak);


            // If too much time before opening say in what time will be this opening (not how much left before)
            if ($minutesTo >= $forecastThreshold) {
                return [
                    'type' => $breakType ? self::STATUS_WILL_OPEN_AT_TIME_FROM_BREAK : self::STATUS_WILL_OPEN_AT_TIME,
                    'breakType' => $breakType,
                    'time' => $next['time']
                ];
            }

            return [
                'type' => $breakType ? self::STATUS_WILL_OPEN_IN_TIME_FROM_BREAK : self::STATUS_WILL_OPEN_IN_TIME,
                'breakType' => $breakType,
                'minutesTo' => $minutesTo
            ];
        }

        // will open tomorrow
        if ($days == 1) {
            return [
                'type' => self::STATUS_WILL_OPEN_TOMORROW_AT_TIME,
                'time' => $next['time']
            ];
        }

        // will open the day after tomorrow
        if ($days == 2 && !$isWeekendToday) {
            return [
                'type' => self::STATUS_WILL_OPEN_DAY_AFTER_TOMORROW_AT_TIME,
                'time' => $next['time']
            ];
        }

        // will open after 2+ days
        return [
            'type' => self::STATUS_WILL_OPEN_AT_DAY_AT_TIME,
            'day' => $this->utils->getDayFromToday($now['day'], $next['dayOffset']),
            'time' => $next['time']
        ];
    }

    /**
     * @param $intervals
     * @return array
     */
    function getWorkInterval($intervals)
    {
        $first = current($intervals);
        $last = end($intervals);

        return ['from' => $first['from'], 'to'=>$last['to']];
    }

    /**
     * @param $intervals
     * @return array
     */
    function getFirstBreakInterval($intervals)
    {
        if(count($intervals) == 1)
            return [];

        $break = [];
        foreach ($intervals as $item) {
            if(isset($break['from'])) {
                $break['to'] = $item['from'];
                return $break;
            } else
                $break['from'] = $item['to'];
        }

        return [];
    }

    /**
     * @param array $workTime
     * @return array
     */
    function formatFromInput($workTime)
    {
        $result = [];
        foreach ($workTime as $day => $value){
            if(!isset($value['break_hours']))
                $result[$day]['working_hours'][] = $value['working_hours'];
            else{
                $result[$day]['working_hours'] = [
                    ['from'=> $value['working_hours']['from'], 'to'=>$value['break_hours']['from']],
                    ['from'=> $value['break_hours']['to'], 'to'=>$value['working_hours']['to']]
                ];
            }

        }
        return $result;
    }

    /**
     * @param array $worktime
     * @return string  // Пн - Пт, 11:00 - 20:00, без перерыва
     */
    public function toString($worktime)
    {
        if ($this->is24x7($worktime))
            return 'Круглосуточно';

        $data = [];
        foreach (self::DAY_KEYS as $day) {
            if(!isset($worktime[$day]['working_hours'])) {
                $data['выходной'][] = self::RU_WEEKDAYS_SHORT[$day];
                continue;
            }
            $work = $this->getWorkInterval($worktime[$day]['working_hours']);
            if(isset($worktime[$day]['break_hours']))
                $break = $this->getFirstBreakInterval($worktime[$day]['break_hours']);
            else
                $break = false;

            $item = $work['from'] . ' - ' . $work['to'];
            $item .= $break ? ', перевыв '. $break['from'] . ' - ' . $break['to'] : ', без перерыва';
            $data[$item][] = self::RU_WEEKDAYS_SHORT[$day];
        }

        $result = [];
        foreach ($data as $value => $days) {
            if(count($days) > 2) {
                $result[] = current($days) . '-' . end($days) . ', ' . $value;
            } else {
                $result[] = implode(', ', $days) . ', ' . $value;
            }
        }

        return implode('. ', $result);
    }
}

